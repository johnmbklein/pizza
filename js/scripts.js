//BUSSINESS LOGIC

//shopping cart
var cart = {
  contents: [],
  address: {},
  email: "",
  delivery: 0,
  total: 0
};
//end shopping cart

//address constructor
function Address(name, street, city, state, zip) {
  this.name = name;
  this.street = street;
  this.city = city;
  this.state = state;
  this.zip = zip;
  this.fullAddress = "";
};
//end address constructor

//pizza constructor
function Pizza(size, crust, toppings) {
  this.size = size;
  this.crust = crust;
  this.toppings = toppings;
};
//end pizza constructor

//NOTE: Without even calling this method, including this code produces a jquery error "Uncaught TypeError jquery-1.12.0.min.js:2." What's going on?
//cart total calculator
// cart.prototype.totalPrice = function (){
//   var totalP = 0
//   for (var i = 0; i < this.contents.length; i++) {
//     total += this.contents[i].price;
//   }
// };
//end cart total calculator

//full address method -- returns full address from address components
Address.prototype.makeFullAddress = function() {
  this.fullAddress = this.name + '<br>' + this.street + '<br>' + this.city + ", " + this.state + " " + this.zip;
};
//end full address method

//pizza price method -- returns total price
Pizza.prototype.price = function() {
  var price;

  //calculate base price based on size
  switch (this.size) {
    case 10:
      price = 9.99;
      break;
    case 14:
      price = 13.99;
      break;
    case 18:
      price = 17.99;
      break;
    default:
  }

  //calculate price of toppings
  price += this.toppings.length * .5;

  //additional charge for delivery
  if (cart.delivery === 1) {
    price += 2.99;
  }

  //round to two decimal places

  //return total price
  return price;
};
//end pizza price method


//UI LOGIC

//function to build order page
var buildOrder = function (){

  var orderTotalPrice = 0;

  //reset output eachtime function is run
  $('#output').empty();

  //takeout or delivery
  if (cart.delivery === 0) {
    $('#takeout-delivery').html('Takeout');
  } else {
    $('#takeout-delivery').html('Delivery');
    //if delivery add delivery address to order page
    $('.delivery-address').html('<p>Will be delivered to:<br>' + cart.address.fullAddress + '</p><br>')
  }

  //add email address to output
  $('#email-output').html(cart.email);

  //list pizzas
  //iterate through each pizza
  for (var i = 0; i < cart.contents.length; i++) {

    //add price of each pizza to orderTotalPrice
    orderTotalPrice += cart.contents[i].price();


    //make plural if multiple toppings
    var pluralizer = ''
    if (cart.contents[i].toppings.length !== 1) {
      pluralizer = 's'
    }

    //generate html output for each pizza
    var output = '';
    output += '<div class="pizza' + i + '"><h4>' + cart.contents[i].size + '" ';
    output += cart.contents[i].crust.toLowerCase() + ' pizza with ';
    output += cart.contents[i].toppings.length + ' topping' + pluralizer;
    output += '</h4><ul id="toppings' + i + '"></ul>';
    output += '$' + cart.contents[i].price().toFixed(2) + '</div><br>';
    $('#output').append(output);

    //iterate through each topping
    for (var j = 0; j < cart.contents[i].toppings.length; j++) {
      //build html output for each topping
      var toppingOutput = '<li>';
      toppingOutput += cart.contents[i].toppings[j];
      toppingOutput += '</li>';
      $('#toppings' + i).append(toppingOutput);
    }
    //end iterate through each topping

  }
  //end iterate through each pizza

  //add total price to order page
  $('#total').html(orderTotalPrice.toFixed(2));

};
//end order page

//funtion to update badge
var updateBadge = function (){
  $('.badge').html(cart.contents.length)
};
//end function to update badge

//page generation functions

  //email page
var emailPage = function(){
  $('#email-page').removeClass('hidden');
  $('#address-form').addClass('hidden');
  $('#pizza-builder-page').addClass('hidden');
  $('#another-pizza-page').addClass('hidden');
  $('#order-page').addClass('hidden');
};

  //address form page
var addressPage = function(){
  $('#email-page').addClass('hidden');
  $('#address-form').removeClass('hidden');
  $('#pizza-builder-page').addClass('hidden');
  $('#another-pizza-page').addClass('hidden');
  $('#order-page').addClass('hidden');
};

  //pizza maker page
var pizzaPage = function(){
  $('input:checkbox').removeAttr('checked');
  $('#email-page').addClass('hidden');
  $('#address-form').addClass('hidden');
  $('#pizza-builder-page').removeClass('hidden');
  $('#another-pizza-page').addClass('hidden');
  $('#order-page').addClass('hidden');
};

  //another pizza page
var anotherPizzaPage = function(){
  $('#email-page').addClass('hidden');
  $('#address-form').addClass('hidden');
  $('#pizza-builder-page').addClass('hidden');
  $('#another-pizza-page').removeClass('hidden');
  $('#order-page').addClass('hidden');
};

  //order page
var orderPage = function(){
  $('#email-page').addClass('hidden');
  $('#address-form').addClass('hidden');
  $('#pizza-builder-page').addClass('hidden');
  $('#another-pizza-page').addClass('hidden');
  $('#order-page').removeClass('hidden');
};

//end page geneation fuctions

//on load, generate email page
emailPage();


//Capture user input via jQuery

//delevery/takeout?
$('#delivery').click(function(){
  cart.delivery = 1;
  cart.email = $('#email').val();
  addressPage();
});
  //go to address form

$('#takeout').click(function(){
  cart.delivery = 0;
  cart.email = $('#email').val();
  pizzaPage();
});
  //skip address form; got to pizza page

//address page
$('#address-form').submit(function(event){
  event.preventDefault();
  var customerAddress = new Address ($('#name').val(), $('#address').val(), $('#city').val(), $('#state').val(), $('#zip').val());
  customerAddress.makeFullAddress();
  cart.address = customerAddress;
  pizzaPage();
});
  //go to puzza page

//pizza page
$('#pizza-form').submit(function(event){
  event.preventDefault();
  var toppings = [];
  $('#pizza-form input:checkbox:checked').each(function(){
    toppings.push($(this).val());
  });
  var pizza = new Pizza(parseInt($('#size').val()), $('#crust').val(), toppings);
  cart.contents.push(pizza);
  anotherPizzaPage();
  updateBadge();
});

//another pizza? page
$('.add-pizza').click(function(){
  pizzaPage();
});
  //go to pizza page

$('#checkout').click(function(){
  buildOrder();
  orderPage();
});
  //go to order page

//order page
$('.place-order').click(function(){
  alert('Your order has been placed.')
});

// $('.pizza').click(function(){
//   $(this).find('ul').toggleClass('hidden');
//   console.log('click');
// });
