# John's Dope Pizza

#### Website that allows users to build and order pizzas for delivery or takeout

#### By John Klein

## Description

Example website for a pizza shop. Includes functionality that allows users to customize and order pizzas for either delivery or takeout.

## Setup/Installation Requirements

No special setup required! Just go to http://johnmbklein.github.io/pizza/ and check it out.

## Known Bugs

No known bugs at this time.

## Support and contact details

Comments, suggestions, or questions? Email john@email.com.

## Technologies Used

Website was created with HTML, CSS, bootstrap, Javascrpit, and jQuery.

### License

Licensed under the GNU public license. See the license file for more information

Copyright (c) 2015 John Klein
